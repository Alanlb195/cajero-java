package CajeroDB4O;

import java.util.List;
import java.util.ArrayList;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class DataBase4O {

	private ObjectContainer db = null;

	// Open the database
	private void abrirDatabase() {
		db = Db4oEmbedded.openFile("registroClientes");
	}

	// Close the database
	private void cerrarDatabase() {
		db.close();
	}

	// Create method
	void insertarRegistro(Persona p) {
		abrirDatabase();
		db.store(p);
		cerrarDatabase();
	}

	// Read method, all the objects
	public List<Persona> seleccionarPersonas() {
		abrirDatabase();
		ObjectSet<Object> listaPersonas = db.queryByExample(Persona.class);
		List<Persona> lp = new ArrayList<>();

		for (Object listaPersonas1 : listaPersonas) {
			lp.add((Persona) listaPersonas1);
		}

		cerrarDatabase();

		return lp;
	}

	// Read method, only one object
	public Persona seleccionarPersona(Persona p) {
		abrirDatabase();
		ObjectSet<Object> resultado = db.queryByExample(p);
		Persona persona = (Persona) resultado.next();
		cerrarDatabase();
		return persona;
	}

	// Put money to the account
	public void depositarFondos(int numTarj, int nip, int fondos) {
		abrirDatabase();
		Persona p = new Persona();
		p.setNumTarj(numTarj);
		ObjectSet<Object> resultado = db.queryByExample(p);
		Persona preResultado = (Persona) resultado.next();

		if (preResultado.getNip() == nip) {
			preResultado.setFondos(preResultado.getFondos() + fondos);
			db.store(preResultado);
			System.out.println("Transaction successfull!");
		} else {
			System.out.println("bad password");
		}
		cerrarDatabase();
	}

	// Get the money of the account method
	public void retirarFondos(int numTarj, int nip, int fondos) {
		abrirDatabase();
		Persona p = new Persona();
		p.setNumTarj(numTarj);
		ObjectSet<Object> resultado = db.queryByExample(p);
		Persona preResultado = (Persona) resultado.next();

		if (preResultado.getNip() == nip) {
			if ( preResultado.getFondos() >= fondos) {
				preResultado.setFondos(preResultado.getFondos() - fondos);
				db.store(preResultado);
				System.out.println("Transaction successfull!");
			} else {
				System.out.println("Not enough of money!");
			}
		} else {
			System.out.println("bad password");
		}
		cerrarDatabase();
	}

	// Delete method
	public void eliminarPersona(int numTarj) {
		abrirDatabase();
		Persona p = new Persona();
		p.setNumTarj(numTarj);
		ObjectSet<Object> resultado = db.queryByExample(p);
		Persona preResultado = (Persona) resultado.next();
		db.delete(preResultado);
		System.out.println("Cuenta borrada con �xito");
		cerrarDatabase();
	}

}
