package CajeroDB4O;

public class Persona {

	private int numTarj;
	private String nombrePersona;
	private String apellidoPPersona;
	private String apellidoMPersona;
	private int nip;
	private int fondos;

	public int getNumTarj() {
		return numTarj;
	}

	public void setNumTarj(int numTarj) {
		this.numTarj = numTarj;
	}

	public String getNombrePersona() {
		return nombrePersona;
	}

	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}

	public String getApellidoPPersona() {
		return apellidoPPersona;
	}

	public void setApellidoPPersona(String apellidoPPersona) {
		this.apellidoPPersona = apellidoPPersona;
	}

	public String getApellidoMPersona() {
		return apellidoMPersona;
	}

	public void setApellidoMPersona(String apellidoMPersona) {
		this.apellidoMPersona = apellidoMPersona;
	}

	public int getNip() {
		return nip;
	}

	public void setNip(int nip) {
		this.nip = nip;
	}

	public int getFondos() {
		return fondos;
	}

	public void setFondos(int fondos) {
		this.fondos = fondos;
	}

	public String toString() {
		return "Persona { Tarj: " + numTarj + ", " + nombrePersona + ", " + apellidoPPersona + ", " + apellidoMPersona
				+ ", Nip: " + nip + ", $ " + fondos + " }";
	}

}
