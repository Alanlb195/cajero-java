package CajeroDB4O;

import java.util.Scanner;
import java.security.SecureRandom;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		DataBase4O d = new DataBase4O();
		Persona p = new Persona();
		Scanner sc = new Scanner(System.in);
		SecureRandom rand = new SecureRandom();

		do {

			System.out.println("////////////////////////////////////////////////"
					+ "\nWelcome to the ATM\n\n-- Options:\n" + "1) Crear cuenta " + "\n2) Consultar cuenta"
					+ "\n3) Depositar" + "\n4) Retirar" + "\n5) Listar cuentas (Admin)"
					+ "\n6) Eliminar cuenta (Admin)\n" + "////////////////////////////////////////////////");
			int ans = sc.nextInt();

			if (ans == 1) {
				// CREAR CUENTA
				p.setNumTarj(rand.nextInt(2000));
				System.out.println("Nombre:");
				p.setNombrePersona(sc.next());
				System.out.println("Ape pat:");
				p.setApellidoPPersona(sc.next());
				System.out.println("Ape mat:");
				p.setApellidoMPersona(sc.next());
				System.out.println("Nip:");
				p.setNip(sc.nextInt());
				System.out.println("Fondos:");
				p.setFondos(sc.nextInt());
				d.insertarRegistro(p);
				System.out.println(p);
			} else if (ans == 2) {
				// CONSULTAR CUENTA
				System.out.println("NumTarj:");
				p.setNumTarj(sc.nextInt());
				p = d.seleccionarPersona(p);
				System.out.println(p);
			} else if (ans == 3) {
				// DEPOSITAR FONDOS
				System.out.println("NumTarj:");
				int tarjeta = sc.nextInt();
				System.out.println("Nip:");
				int nip = sc.nextInt();
				System.out.println("Fondos:");
				int fondos = sc.nextInt();
				d.depositarFondos(tarjeta, nip, fondos);
			} else if (ans == 4) {
				// RETIRAR FONDOS
				System.out.println("NumTarj:");
				int tarjeta = sc.nextInt();
				System.out.println("Nip:");
				int nip = sc.nextInt();
				System.out.println("Fondos:");
				int fondos = sc.nextInt();
				d.retirarFondos(tarjeta, nip, fondos);
			} else if (ans == 5) {
				// MOSTRAR TODAS LAS CUENTAS
				List<Persona> lp = d.seleccionarPersonas();
				for (Persona persona : lp) {
					System.out.println(persona);
				}
			} else if (ans == 6) {
				// ELIMINAR CUENTA
				System.out.println("La eliminación de cuentas no se puede deshacer.\nIntroduce tu numero de tarjeta");
				d.eliminarPersona(sc.nextInt());
			}
			else {
				System.out.println("Opción invalida");
			}

		} // do

		while (true);

		// ELIMINAR PERSONA
		// r.eliminarPersona(1587);

	}

}
